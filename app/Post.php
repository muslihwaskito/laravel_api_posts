<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
	protected $table = 'posts';
    protected $fillable = [
        'title', 'content', 'active','created_by', 'updated_by'
    ];

	public function user()
    {
    	return $this->belongsTo(User::class,'created_by');
    }
}
