<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Passport\Client;
use Illuminate\Support\Facades\Auth;
use Route;
use Validator;
use App\Post;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->client = Client::find('2');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::select('title')->get();
        $response = [
            'code' => 200,
            'message' => 'success hit data',
            'data' => $post
        ];
        return response()->json($response)->setStatusCode(200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'content' => 'required'
            ]
        );
        if ($validator->fails()) {
            $errors = collect($validator->errors())->map(function($item,$key){
                return $item[0];
            });
            $response = [
                'code' => 422,
                'message' => 'error request search',
                'error_message' => array_values($errors->toArray()),
                'data' => []
            ];
            return response()->json($response)->setStatusCode(422);
        }
        $post = $request->only('title','content');
        $post['active'] = 0;
        $post['created_by'] = Auth::user()->id;
        $insert = Post::create($post);
        $response = [
            'code' => 200,
            'message' => 'success insert data'
        ];
        return response()->json($response)->setStatusCode(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('id',$id)->first();
        $response = [
            'code' => 200,
            'message' => 'success hit data',
            'data' => $post
        ];
        return response()->json($response)->setStatusCode(200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'content' => 'required'
            ]
        );
        if ($validator->fails()) {
            $errors = collect($validator->errors())->map(function($item,$key){
                return $item[0];
            });
            $response = [
                'code' => 422,
                'message' => 'error request search',
                'error_message' => array_values($errors->toArray()),
                'data' => []
            ];
            return response()->json($response)->setStatusCode(422);
        }
        $post = $request->only('title','content');
        $post['updated_by'] = Auth::user()->id;
        $insert = Post::where('id', $id)->update($post);
        $find = Post::find($insert);
        $response = [
            'code' => 200,
            'message' => 'success update data',
            'data' => $find
        ];
        return response()->json($response)->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request)
    {
        $this->validate($request, [
                'username' => 'required',
                'password' => 'required|string|min:6',  
            ]);
        $params = [
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $request->username,
            'password' => $request->password,
            'scope' => '*',
        ];
        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST',$params);
        // dd($params);
        // dd($request->request, $proxy);
        return Route::dispatch($proxy);
    }
}
