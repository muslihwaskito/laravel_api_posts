<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
          'username' => 'muhammadmuslih',
          'fullname' => 'muhammad muslih waskito',
          'password' => bcrypt('secret'),
          'active' => 1
          ]);
    }
}
